<<<<<<< HEAD:flakytests/special/08-clock-triggered-0.t
#!/bin/bash
# THIS FILE IS PART OF THE CYLC SUITE ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
# 
=======
#!/usr/bin/env bash
# THIS FILE IS PART OF THE CYLC WORKFLOW ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
#
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/special/08-clock-triggered-0.t
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------
# Test clock triggering is working, with no offset argument
# https://github.com/cylc/cylc-flow/issues/1417
. "$(dirname "$0")/test_header"
#-------------------------------------------------------------------------------
set_test_number 4
#-------------------------------------------------------------------------------
<<<<<<< HEAD:flakytests/special/08-clock-triggered-0.t
install_suite "${TEST_NAME_BASE}" "${TEST_NAME_BASE}"
#-------------------------------------------------------------------------------
run_ok "${TEST_NAME_BASE}-validate" \
    cylc validate "${SUITE_NAME}" -s START="$(date '+%Y%m%dT%H%z')" \
    -s HOUR="$(date '+%H')" -s 'UTC_MODE=False' -s 'TIMEOUT=PT0.2M'
#-------------------------------------------------------------------------------
run_ok "${TEST_NAME_BASE}-run-now" \
    cylc run --debug --no-detach "${SUITE_NAME}" \
    -s START="$(date '+%Y%m%dT%H%z')" \
    -s HOUR="$(date '+%H')" -s 'UTC_MODE=False' -s 'TIMEOUT=PT0.2M'
#-------------------------------------------------------------------------------
=======
install_workflow "${TEST_NAME_BASE}" "${TEST_NAME_BASE}"
#-------------------------------------------------------------------------------
run_ok "${TEST_NAME_BASE}-validate" \
    cylc validate "${WORKFLOW_NAME}" -s "START='$(date '+%Y%m%dT%H%z')'" \
    -s "HOUR='$(date '+%H')'" -s 'UTC_MODE="False"' -s 'TIMEOUT="PT0.2M"'
#-------------------------------------------------------------------------------
run_ok "${TEST_NAME_BASE}-run-now" \
    cylc play --debug --no-detach "${WORKFLOW_NAME}" \
    -s "START='$(date '+%Y%m%dT%H%z')'" \
    -s "HOUR='$(date '+%H')'" -s 'UTC_MODE="False"' -s 'TIMEOUT="PT0.2M"'
#-------------------------------------------------------------------------------
delete_db
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/special/08-clock-triggered-0.t
NOW="$(date '+%Y%m%dT%H')"
START="$(cylc cycle-point "${NOW}" --offset-hour='-10')$(date '+%z')"
HOUR="$(cylc cycle-point "${NOW}" --offset-hour='-10' --print-hour)"
run_ok "${TEST_NAME_BASE}-run-past" \
<<<<<<< HEAD:flakytests/special/08-clock-triggered-0.t
    cylc run --debug --no-detach "${SUITE_NAME}" -s START="${START}" \
    -s HOUR="${HOUR}" -s 'UTC_MODE=False' -s 'TIMEOUT=PT1M'
#-------------------------------------------------------------------------------
=======
    cylc play --debug --no-detach "${WORKFLOW_NAME}" -s "START='${START}'" \
    -s "HOUR='${HOUR}'" -s 'UTC_MODE="False"' -s 'TIMEOUT="PT1M"'
#-------------------------------------------------------------------------------
delete_db
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/special/08-clock-triggered-0.t
NOW="$(date '+%Y%m%dT%H')"
START="$(cylc cycle-point "${NOW}" --offset-hour='10')$(date '+%z')"
HOUR="$(cylc cycle-point "${NOW}" --offset-hour='10' --print-hour)"
run_fail "${TEST_NAME_BASE}-run-later" \
<<<<<<< HEAD:flakytests/special/08-clock-triggered-0.t
    cylc run --debug --no-detach "${SUITE_NAME}" -s START="${START}" \
    -s HOUR="${HOUR}" -s 'UTC_MODE=False' -s 'TIMEOUT=PT0.2M'
#-------------------------------------------------------------------------------
purge_suite "${SUITE_NAME}"
=======
    cylc play --debug --no-detach "${WORKFLOW_NAME}" -s START="${START}" \
    -s "HOUR='${HOUR}'" -s 'UTC_MODE="False"' -s 'TIMEOUT="PT0.2M"'
#-------------------------------------------------------------------------------
purge
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/special/08-clock-triggered-0.t
exit
