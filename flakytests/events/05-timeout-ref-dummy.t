<<<<<<< HEAD:flakytests/events/05-timeout-ref-dummy.t
#!/bin/bash
# THIS FILE IS PART OF THE CYLC SUITE ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
# 
=======
#!/usr/bin/env bash
# THIS FILE IS PART OF THE CYLC WORKFLOW ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
#
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/events/05-timeout-ref-dummy.t
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------
<<<<<<< HEAD:flakytests/events/05-timeout-ref-dummy.t
# Validate and run the suite reference test dummy timeout suite
=======
# Validate and run the workflow reference test dummy timeout workflow
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/events/05-timeout-ref-dummy.t
. "$(dirname "$0")/test_header"
#-------------------------------------------------------------------------------
set_test_number 3
#-------------------------------------------------------------------------------
<<<<<<< HEAD:flakytests/events/05-timeout-ref-dummy.t
install_suite "${TEST_NAME_BASE}" "${TEST_NAME_BASE}"
#-------------------------------------------------------------------------------
run_ok "${TEST_NAME_BASE}-validate" cylc validate "${SUITE_NAME}"
#-------------------------------------------------------------------------------
TEST_NAME="${TEST_NAME_BASE}-run"
RUN_MODE="$(basename "$0" | sed "s/.*-ref-\(.*\).t/\1/g")"
suite_run_fail "${TEST_NAME}" \
    cylc run --reference-test --mode="${RUN_MODE}" --debug --no-detach \
    "${SUITE_NAME}"
grep_ok "WARNING - suite timed out after PT1S" "${TEST_NAME}.stderr"
#-------------------------------------------------------------------------------
purge_suite "${SUITE_NAME}"
=======
install_workflow "${TEST_NAME_BASE}" "${TEST_NAME_BASE}"
#-------------------------------------------------------------------------------
run_ok "${TEST_NAME_BASE}-validate" cylc validate "${WORKFLOW_NAME}"
#-------------------------------------------------------------------------------
TEST_NAME="${TEST_NAME_BASE}-run"
RUN_MODE="$(basename "$0" | sed "s/.*-ref-\(.*\).t/\1/g")"
workflow_run_fail "${TEST_NAME}" \
    cylc play --mode="${RUN_MODE}" --debug --no-detach "${WORKFLOW_NAME}"
grep_ok "WARNING - workflow timed out after PT1S" "${TEST_NAME}.stderr"
#-------------------------------------------------------------------------------
purge
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/events/05-timeout-ref-dummy.t
exit
