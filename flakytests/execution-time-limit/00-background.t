<<<<<<< HEAD:flakytests/execution-time-limit/00-background.t
#!/bin/bash
# THIS FILE IS PART OF THE CYLC SUITE ENGINE.
=======
#!/usr/bin/env bash
# THIS FILE IS PART OF THE CYLC WORKFLOW ENGINE.
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/execution-time-limit/00-background.t
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------
# Test execution time limit, background/at job
JOB_RUNNER="${0##*\/??-}"
export REQUIRE_PLATFORM="runner:${JOB_RUNNER%%.t}"
. "$(dirname "$0")/test_header"
set_test_number 4

<<<<<<< HEAD:flakytests/execution-time-limit/00-background.t
set_test_number 4
skip_darwin 'atrun hard to configure on Mac OS'
install_suite "${TEST_NAME_BASE}" "${TEST_NAME_BASE}"

CYLC_TEST_BATCH_SYS=${TEST_NAME_BASE##??-}

run_ok "${TEST_NAME_BASE}-validate" \
    cylc validate \
    -s "CYLC_TEST_BATCH_SYS=${CYLC_TEST_BATCH_SYS}" \
    "${SUITE_NAME}"
suite_run_fail "${TEST_NAME_BASE}-run" \
    cylc run --reference-test --debug --no-detach \
    -s "CYLC_TEST_BATCH_SYS=${CYLC_TEST_BATCH_SYS}" \
    "${SUITE_NAME}"
=======
install_workflow "${TEST_NAME_BASE}" "${TEST_NAME_BASE}"

run_ok "${TEST_NAME_BASE}-validate" \
    cylc validate "${WORKFLOW_NAME}"
workflow_run_fail "${TEST_NAME_BASE}-run" \
    cylc play --reference-test --debug --no-detach --abort-if-any-task-fails "${WORKFLOW_NAME}"
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/execution-time-limit/00-background.t

LOGD="${RUN_DIR}/${WORKFLOW_NAME}/log/job/1/foo"
grep_ok '# Execution time limit: 5.0' "${LOGD}/01/job"
<<<<<<< HEAD:flakytests/execution-time-limit/00-background.t
grep_ok 'CYLC_JOB_EXIT=\(ERR\|XCPU\)' "${LOGD}/01/job.status"
=======
grep_ok 'CYLC_JOB_EXIT=XCPU' "${LOGD}/01/job.status"
>>>>>>> upstream/8.0_b2:tests/flakyfunctional/execution-time-limit/00-background.t

purge
exit
