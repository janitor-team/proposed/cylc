#!/usr/bin/env python3
<<<<<<< HEAD:cylc/flow/scripts/cylc_function_run.py
# THIS FILE IS PART OF THE CYLC SUITE ENGINE.
=======
# THIS FILE IS PART OF THE CYLC WORKFLOW ENGINE.
>>>>>>> upstream/8.0_b2:cylc/flow/scripts/function_run.py
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""USAGE: cylc function-run <name> <json-args> <json-kwargs> <src-dir>

(This command is for internal use.)

Run a Python function "<name>(*args, **kwargs)" in the process pool. It must be
defined in a module of the same name. Positional and keyword arguments must be
passed in as JSON strings. <src-dir> is the workflow source dir, needed to find
local xtrigger modules.
"""
import sys

from cylc.flow.subprocpool import run_function

<<<<<<< HEAD:cylc/flow/scripts/cylc_function_run.py

def main():
    if sys.argv[1] in ["help", "--help"] or len(sys.argv) != 5:
        print(__doc__)
        sys.exit(0)
    run_function(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
=======
INTERNAL = True


def main(*api_args):
    if api_args:
        args = [None] + list(api_args)
    else:
        args = sys.argv
    if args[1] in ["help", "--help"] or len(args) != 5:
        print(__doc__)
        sys.exit(0)
    run_function(args[1], args[2], args[3], args[4])
>>>>>>> upstream/8.0_b2:cylc/flow/scripts/function_run.py


if __name__ == "__main__":
    main()
