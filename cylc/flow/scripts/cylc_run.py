<<<<<<< HEAD:cylc/flow/scripts/cylc_run.py
#!/usr/bin/env python3
# THIS FILE IS PART OF THE CYLC SUITE ENGINE.
=======
# THIS FILE IS PART OF THE CYLC WORKFLOW ENGINE.
>>>>>>> upstream/8.0_b2:cylc/flow/main_loop/reset_bad_hosts.py
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
<<<<<<< HEAD:cylc/flow/scripts/cylc_run.py
"""CLI of "cylc run". See cylc.flow.scheduler_cli for detail."""
from cylc.flow.scheduler_cli import main as scheduler_main


def main():
    scheduler_main(is_restart=False)


if __name__ == "__main__":
    main()
=======
"""Reset the list of bad hosts."""

from cylc.flow.main_loop import periodic


@periodic
async def reset_bad_hosts(scheduler, _):
    """Empty bad_hosts."""
    scheduler.task_events_mgr.reset_bad_hosts()
>>>>>>> upstream/8.0_b2:cylc/flow/main_loop/reset_bad_hosts.py
