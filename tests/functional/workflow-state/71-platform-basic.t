<<<<<<< HEAD:tests/functional/workflow-state/71-platform-basic.t
#!/bin/bash
# THIS FILE IS PART OF THE CYLC SUITE ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
# 
=======
#!/usr/bin/env bash
# THIS FILE IS PART OF THE CYLC WORKFLOW ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
#
>>>>>>> upstream/8.0_b2:tests/functional/cylc-get-config/02-cycling.t
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------
<<<<<<< HEAD:tests/functional/workflow-state/71-platform-basic.t
# Test validating platforms in suite.
. "$(dirname "$0")/test_header"

set_test_number 1

TEST_NAME="${TEST_NAME_BASE}-val"

cat >'suite.rc' <<'__SUITE_RC__'
[meta]
    title = "Test validation of simple multiple inheritance"

    description = """Bug identified at 5.1.1-314-g4960684."""

[scheduling]
[[graph]]
R1 = """foo"""
[runtime]
[[foo]]
platform=lewis


__SUITE_RC__

run_ok "${TEST_NAME}" cylc validate suite.rc
=======
# Test "cylc config" on a cycling workflow, result should validate.
. "$(dirname "$0")/test_header"

set_test_number 2

init_workflow "${TEST_NAME_BASE}" "${TEST_SOURCE_DIR}/${TEST_NAME_BASE}/flow.cylc"

run_ok "${TEST_NAME_BASE}" cylc config "${WORKFLOW_NAME}"
run_ok "${TEST_NAME_BASE}-validate" \
    cylc validate --check-circular "${TEST_NAME_BASE}.stdout"
purge
exit
>>>>>>> upstream/8.0_b2:tests/functional/cylc-get-config/02-cycling.t
