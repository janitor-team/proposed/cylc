<<<<<<< HEAD:tests/functional/repeated-items/05-bad-syntax.t
#!/bin/bash
# THIS FILE IS PART OF THE CYLC SUITE ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
# 
=======
#!/usr/bin/env bash
# THIS FILE IS PART OF THE CYLC WORKFLOW ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
#
>>>>>>> upstream/8.0_b2:tests/functional/validate/06-implicit-missing.t
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------
<<<<<<< HEAD:tests/functional/repeated-items/05-bad-syntax.t
# Test bad syntax in "script" value leads to submit fail.
. "$(dirname "${0}")/test_header"
#-------------------------------------------------------------------------------
set_test_number 2
#-------------------------------------------------------------------------------
install_suite "${TEST_NAME_BASE}" "${TEST_NAME_BASE}"
TEST_NAME="${TEST_NAME_BASE}-advanced-validate"
run_ok "${TEST_NAME}" cylc validate "${SUITE_NAME}"
TEST_NAME="${TEST_NAME_BASE}-advanced-run"
run_ok "${TEST_NAME}" cylc run "${SUITE_NAME}" --reference-test --debug --no-detach
#-------------------------------------------------------------------------------
purge_suite "${SUITE_NAME}"
exit
=======
# Test validation of workflow for tasks without runtime entries
# (accidental implicit tasks)
. "$(dirname "$0")/test_header"
#-------------------------------------------------------------------------------
set_test_number 1
#-------------------------------------------------------------------------------
init_workflow "${TEST_NAME_BASE}" << __FLOW__
[scheduling]
    [[graph]]
        R1 = "foo => bar"
[runtime]
    [[foo]]
__FLOW__
#-------------------------------------------------------------------------------
TEST_NAME="${TEST_NAME_BASE}-val"
run_fail "${TEST_NAME}" cylc validate "${WORKFLOW_NAME}"
#-------------------------------------------------------------------------------
purge
>>>>>>> upstream/8.0_b2:tests/functional/validate/06-implicit-missing.t
