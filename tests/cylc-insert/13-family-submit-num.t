<<<<<<< HEAD:tests/cylc-insert/13-family-submit-num.t
#!/bin/bash
# THIS FILE IS PART OF THE CYLC SUITE ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
# 
=======
#!/usr/bin/env bash
# THIS FILE IS PART OF THE CYLC WORKFLOW ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
#
>>>>>>> upstream/8.0_b2:tests/functional/triggering/17-suicide-multi.t
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------
<<<<<<< HEAD:tests/cylc-insert/13-family-submit-num.t
# Test "cylc insert" family where a task in the family have previous jobs.
# The submit number of the task should increment.
. "$(dirname "$0")/test_header"
set_test_number 3

install_suite "${TEST_NAME_BASE}" "${TEST_NAME_BASE}"

run_ok "${TEST_NAME_BASE}-validate" cylc validate "${SUITE_NAME}"
suite_run_ok "${TEST_NAME_BASE}-run" \
    cylc run "${SUITE_NAME}" --no-detach --debug --reference-test
run_ok "${TEST_NAME_BASE}-bar-02" test -d "${SUITE_RUN_DIR}/log/job/1/bar/02"
=======
# Test "or" outputs from same task triggering suicide triggering
. "$(dirname "$0")/test_header"
set_test_number 3
install_workflow "${TEST_NAME_BASE}" "${TEST_NAME_BASE}"

run_ok "${TEST_NAME_BASE}-validate" cylc validate "${WORKFLOW_NAME}"
workflow_run_ok "${TEST_NAME_BASE}" \
cylc play --reference-test --debug --no-detach "${WORKFLOW_NAME}"
DBFILE="$RUN_DIR/${WORKFLOW_NAME}/log/db"
sqlite3 "${DBFILE}" 'SELECT cycle, name, status FROM task_pool ORDER BY cycle, name;' \
    >'sqlite3.out'
cmp_ok 'sqlite3.out' <'/dev/null'
>>>>>>> upstream/8.0_b2:tests/functional/triggering/17-suicide-multi.t

purge
exit
